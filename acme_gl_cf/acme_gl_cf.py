# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sewer
import toml
import argparse
from acme_gl_cf import gitlab


def main():
    parser = argparse.ArgumentParser(
            description='Get certificate for gitlab+cloudflare sites')
    parser.add_argument(
            'config', metavar='CONFIG_FILE', type=argparse.FileType())
    args = parser.parse_args()

    with args.config as f:
        config = toml.load(f)

    dns_class = sewer.CloudFlareDns(
            CLOUDFLARE_EMAIL=config["cloudflare"]["email"],
            CLOUDFLARE_API_KEY=config["cloudflare"]["api_key"])

    cert, key, acct_key = new_cert(dns_class, config["domain"])
    gitlab.upload_cert(
            config["gitlab"]["repo"],
            config["domain"],
            cert, key,
            config["gitlab"]["token"])


def new_cert(dns_class, domain):
    client = sewer.Client(
            domain_name='www.aravena.org',
            dns_class=dns_class)
    certificate = client.cert()
    certificate_key = client.certificate_key
    account_key = client.account_key

    with open(f'{domain}.certificate.crt', 'w') as certificate_file:
        certificate_file.write(certificate)
    with open(f'{domain}.certificate.key', 'w') as certificate_key_file:
        certificate_key_file.write(certificate_key)
    with open(f'{domain}.account_key.key', 'w') as account_key_file:
        account_key_file.write(account_key)

    return (certificate, certificate_key, account_key)


def renew(dns_class, domain):
    with open('account_key.key', 'r') as account_key_file:
        account_key = account_key_file.read()

    client = sewer.Client(domain_name='example.com',
                          dns_class=dns_class,
                          account_key=account_key)
    certificate = client.renew()
    certificate_key = client.certificate_key

    with open('certificate.crt', 'w') as certificate_file:
        certificate_file.write(certificate)
    with open('certificate.key', 'w') as certificate_key_file:
        certificate_key_file.write(certificate_key)


if __name__ == '__main__':
    main()
