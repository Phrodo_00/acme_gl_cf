# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import requests
import urllib.parse


gitlab_url = "https://gitlab.com/api/v4"


def upload_cert(repo, domain, cert, key, token, verbose=False):
    repo_id = urllib.parse.quote_plus(repo)
    url = f"{gitlab_url}/projects/{repo_id}/pages/domains/{domain}"

    if verbose:
        enable_http_logging()

    print(url)
    r = requests.put(
            url,
            headers={'PRIVATE-TOKEN': token},
            data={
                'certificate': cert,
                'key': key
            }
            )
    print(r)
    return r


def enable_http_logging():
    import logging
    import http.client

    http.client.HTTPConnection.debuglevel = 1

    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True


def main():
    import argparse
    import toml

    parser = argparse.ArgumentParser(
            description='Upload new certificates to gitlab')
    parser.add_argument(
            'config', metavar='CONFIG_FILE', type=argparse.FileType())
    parser.add_argument(
            'cert', metavar='CERT_FILE', type=argparse.FileType())
    parser.add_argument(
            'key', metavar='KEY_FILE', type=argparse.FileType())
    args = parser.parse_args()

    with args.config as f:
        config = toml.load(f)

    with args.cert as f:
        cert = f.read()

    with args.key as f:
        key = f.read()

    upload_cert(
            config["gitlab"]["repo"],
            config["domain"],
            cert, key,
            config["gitlab"]["token"])


if __name__ == '__main__':
    main()
